import React, { useState, useEffect } from 'react'

import NoImages from "./NoImages";
import Image from "./Image";
const Gallery = props => {
  const [ downloadimage] = useState([])

  const results = props.data;
  let images;
  let noImages;
  // map variables to each item in fetched image array and return image component
  if (results.length > 0) {
    images = results.map(image => {
      let farm = image.farm;
      let server = image.server;
      let id = image.id;
      let secret = image.secret;
      let title = image.title;
      let url = `https://farm${farm}.staticflickr.com/${server}/${id}_${secret}_m.jpg`;
      return <><Image url={url} key={id} alt={title} clasName="yolo" /></>;
    });
  } else {
    noImages = <NoImages />; // return 'not found' component if no images fetched
  }
  // useEffect(() => {
  //   downloadImage()
  //     .then((res) => {
  //       downloadimage(res)
  //     })
  //     .catch((e) => {
  //       console.log(e.message)
  //     })
  // }, [])
  // async function downloadImage(imageSrc) {
  //   const image = await fetch(imageSrc)
  //   const imageBlog = await image.blob()
  //   const imageURL = URL.createObjectURL(imageBlog)
  
  //   const link = document.createElement('a')
  //   link.href = imageURL
  //   link.download = 'image file name here'
  //   document.body.appendChild(link)
  //   link.click()
  //   document.body.removeChild(link)
  // }
  return (
    <div>
      <ul>{images}</ul>
      {noImages}
    </div>
  );
};

export default Gallery;
