import React from 'react';
import { NavLink } from 'react-router-dom';

const Navigation = () => {
  return (
    <nav className="main-nav">
      <ul>
        <li><NavLink to="/pizza">Pizza</NavLink></li>
        <li><NavLink to="/games">Games</NavLink></li>
        <li><NavLink to="/cloud computing">Cloud Computing</NavLink></li>
        <li><NavLink to="/esports">E-Sports</NavLink></li>
        <li><NavLink to="/canada">Canada</NavLink></li>
        <li><NavLink to="/maple leaves">Maple Leaves</NavLink></li>
      </ul>
    </nav>
  );
}

export default Navigation;
